﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="AdminMainPage.aspx.cs" Inherits="iControlWorkV5.Pages.AdminMainPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>

        <p>Hello</p>
        <p><asp:Label ID="userlabel" Text="No User" runat="server" /></p>
        <p><asp:Label ID="badgeNolabel" Text="No User" runat="server" /></p>
        <p><asp:Button ID ="loggoutButton" Text ="Logout" runat ="server" OnClick="LogoutEventMethod" /></p>
    
    </div>
        <div>

            <%-- Here the ListView Start --%>           

            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
                SelectCommand="select User.active, User.badgeNo, Role.roleName, User.firstName, User.lastName, Department.departmentName, Division.divisionName, Section.sectionName from User,Department,Division,Section,Role where User.departmentNo=Department.departmentNo and Division.divisionNo=User.divisionNo and Section.sectionNo=User.sectionNo and Role.roleNo=User.roleNo order by badgeNo asc"
                DeleteCommand="DELETE FROM User WHERE badgeNo = @badgeNo">
               
                 <DeleteParameters>
                <asp:Parameter Name="badgeNo" Type="Int32" />
                </DeleteParameters>
            </asp:SqlDataSource>

      
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="badgeNo" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="Vertical" OnSelectedIndexChanged="ViewUserInfo">
                
                
                <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>
                    <asp:CheckBoxField DataField="active" HeaderText="Active" SortExpression="active" />
                    <asp:BoundField DataField="badgeNo" HeaderText="Badge No" ReadOnly="True" SortExpression="badgeNo" />
                    <asp:BoundField DataField="roleName" HeaderText="Role Name" SortExpression="roleName" />
                    <asp:BoundField DataField="firstName" HeaderText="First Name" SortExpression="firstName" />
                    <asp:BoundField DataField="lastName" HeaderText="Last Name" SortExpression="lastName" />
                    <asp:BoundField DataField="departmentName" HeaderText="Department Name" SortExpression="departmentName" />
                    <asp:BoundField DataField="divisionName" HeaderText="Division Name" SortExpression="divisionName" />
                    <asp:BoundField DataField="sectionName" HeaderText="Section Name" SortExpression="sectionName" />
                    <asp:CommandField ShowSelectButton="True" ShowDeleteButton="True" />
                </Columns>

                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
            </asp:GridView>




      
            <%-- Here the listView ends --%>

        </div>
</asp:Content>
