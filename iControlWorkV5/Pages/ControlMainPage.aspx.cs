﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iControlWorkV5.Pages
{
    public partial class ControlMainPage : System.Web.UI.Page
    {
       
            String userName = String.Empty;
             String badgeNumber = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            userName = (String)(Session["uname"]);
            badgeNumber = (String)(Session["badgeNumber"]);

            if (userName == null)
            {
                Response.BufferOutput = true;
                Response.Redirect("../Default.aspx", false);

            }
            else
            {


                userNameLabel.Text = userName;
                badgeNoLabel.Text = Convert.ToString(badgeNumber);
            }

        }


        protected void SelectingRowToViewATrippingReport(object sender, EventArgs e)
        {

            //Accessing BoundField Column

            String reportNo = trippingReportGridView.SelectedRow.Cells[1].Text;

            

            //Accessing TemplateField Column controls

            //string country = (GridView1.SelectedRow.FindControl("lblCountry") as Label).Text;
            Session["reportNo"] = reportNo;
            Response.BufferOutput = true;
            Response.Redirect("TrippingReportView.aspx",false);

            //lblValues.Text = "<b>Name:</b> " + name + " <b>Country:</b> " + country;

        }

        }


        }

