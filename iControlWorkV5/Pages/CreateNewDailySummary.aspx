﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="CreateNewDailySummary.aspx.cs" Inherits="iControlWorkV5.Pages.CreateNewDailySummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style1">
        <tr>
            <td>
               <p><asp:Label ID="dateOfDailySummaryLabel" runat="server" Text="Date"></asp:Label></p>
                <p>
                    <asp:Label ID="userNameLabel" runat="server" Text="User Name"></asp:Label>
&nbsp;
                    <asp:Label ID="badgeNoLabel" runat="server" Text="Badge No"></asp:Label>
                </p> 
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black">
                    <Columns>
                        <asp:BoundField DataField="dateOfTripping" HeaderText="Date" SortExpression="dateOfTripping" />
                        <asp:BoundField DataField="netWorkArea" HeaderText="Area" SortExpression="netWorkArea" />
                        <asp:BoundField DataField="timeOfTripping" HeaderText="Time of Tripping" SortExpression="timeOfTripping" />
                        <asp:BoundField DataField="restorationTimeActual" HeaderText="Restoration Time Actual" SortExpression="restorationTimeActual" />
                        <asp:BoundField DataField="voltagelevel" HeaderText="VoltageLevel" SortExpression="voltagelevel" />
                        <asp:BoundField DataField="substation" HeaderText="Substation" SortExpression="substation" />
                        <asp:BoundField DataField="circuitOrEquipment" HeaderText="Circuit/Equipment" SortExpression="circuitOrEquipment" />
                        <asp:BoundField DataField="areaOfLostSupply" HeaderText="Area of Lost Supply" SortExpression="areaOfLostSupply" />
                        <asp:BoundField DataField="protectionRelay" HeaderText="Protection Relay" SortExpression="protectionRelay" />
                        <asp:BoundField DataField="comment" HeaderText="Comment" SortExpression="comment" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                    <RowStyle BackColor="White" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                </asp:GridView>
                
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
                 SelectCommand="select dateOfTripping, netWorkArea, timeOfTripping, restorationTimeActual, voltagelevel, substation, circuitOrEquipment, areaOfLostSupply, protectionRelay, comment from TrippingReport where (dateOfTripping= @date) and (timeOfTripping > '6:30:00')">
                <SelectParameters> 
                
                <asp:SessionParameter DefaultValue="0" Name="date" SessionField="dateOfDailySummary" Type="DateTime"/>
                  
             
            </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>
                
                <p>
                    <asp:Label ID="Label1" runat="server" Text="Max Demand"></asp:Label>&nbsp;
                    <asp:TextBox ID="maxDemandTextBox" runat="server"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label2" runat="server" Text="Min Demand"></asp:Label>
                &nbsp;
                    <asp:TextBox ID="minDemandTextBox" runat="server"></asp:TextBox>
                </p>

                     <p>
                    <asp:Label ID="Label3" runat="server" Text="Max Temp"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="maxTempTextBox" runat="server"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="minTempLabel" runat="server" Text="Min Temp"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="minTempTextBox" runat="server"></asp:TextBox>
                </p>
                 <p>
                    <asp:Label ID="Label" runat="server" Text="First Enigneer Name"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                    <asp:TextBox ID="firstEngineerNameTextBox" runat="server" Width="270px"></asp:TextBox>
                     </p>
                 <p>
                    <asp:Label ID="Label5" runat="server" Text="Second Enigneer Name"></asp:Label>&nbsp;&nbsp; &nbsp;&nbsp;
                    <asp:TextBox ID="secondEngineerNameTextBox" runat="server" Width="270px"></asp:TextBox>
                     </p>
                <p> <asp:Button ID="createDailySummaryButton" runat="server" Text="Create Daily Summary" OnClick="createDailySummaryButton_Click" /></p>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
