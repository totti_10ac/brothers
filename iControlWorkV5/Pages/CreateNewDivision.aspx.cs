﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iControlWorkV5.Classes;

namespace iControlWorkV5.Pages
{
    public partial class CreateNewDivision : System.Web.UI.Page
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
       // MySql.Data.MySqlClient.MySqlDataReader reader;
        String userName = String.Empty;
        String badgeNumber = String.Empty;



        protected void Page_Load(object sender, EventArgs e)
        {
            userName = (String)(Session["uname"]);
            badgeNumber = (String)(Session["badgeNumber"]);

            if (userName == null)
            {
                Response.BufferOutput = true;
                Response.Redirect("../Default.aspx", false);

            }
            else
            {


                userNameLabel.Text = userName;
                badgeNoLabel.Text = Convert.ToString(badgeNumber);
            }

            //Filling in the dropdown lists

            if (!IsPostBack)
            {
               
                departmentDropDownList.AppendDataBoundItems = true;
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
                
                String strQuery = "select departmentNo, departmentName from Department";
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                cmd = new MySql.Data.MySqlClient.MySqlCommand(strQuery, conn);
               // reader = cmd.ExecuteReader();
                try
                {
                    conn.Open();
                    departmentDropDownList.DataSource = cmd.ExecuteReader();
                    departmentDropDownList.DataTextField = "departmentName";
                    departmentDropDownList.DataValueField = "departmentNo";
                    departmentDropDownList.DataBind();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

        
        }

        protected void createNewDivisionButton_Click(object sender, EventArgs e)
        {
            String messageReturned = String.Empty;

            Division Division = new Division();

            int departmentNo = Convert.ToInt16(departmentDropDownList.SelectedValue);

            messageReturned = Division.CreateNewDivsion(departmentNo, newDivisionTextBox.Text);
            returnedMessageLabel.Text = messageReturned;
            newDivisionTextBox.Text = "";
            Page_Load(null, null);
        }

        
    }
}