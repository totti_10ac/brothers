﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iControlWorkV5.Pages
{
    public partial class ViewDailySummary : System.Web.UI.Page
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        String userName = String.Empty;
        String badgeNumber = String.Empty;
        String queryStr = String.Empty;
        DateTime dateOfDailySummary;
        String DetailDailySummaryDate = String.Empty;




        protected void Page_Load(object sender, EventArgs e)
        {
            userName = (String)(Session["uname"]);
            badgeNumber = (String)(Session["badgeNumber"]);
            // dateOfDailySummary = 

            if (userName == null)
            {
                Response.BufferOutput = true;
                Response.Redirect("../Default.aspx", false);

            }
            else
            {


                userNameLabel.Text = userName;
                badgeNoLabel.Text = Convert.ToString(badgeNumber);
            }

            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();

                queryStr = "SELECT dateOfSummary FROM DailySummary ORDER BY dateOfSummary DESC LIMIT 1";

                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);

                reader = cmd.ExecuteReader();


                if (reader.HasRows && reader.Read())
                {
                    // adl

                    dateOfDailySummary = reader.GetDateTime(reader.GetOrdinal("dateOfSummary"));

                    dateOfUnCreatedDailySummaryTextBox.Text = dateOfDailySummary.AddDays(1).ToShortDateString();


                }
                if (dateOfDailySummary.AddDays(1) >= DateTime.Now)
                {
                    errorMessageLabel.Text = "Sorry it is not possible to create the daily summary!";
                    dateOfUnCreatedDailySummaryTextBox.Text = " No Daily Summary required";

                }

                reader.Close();
                conn.Close();
            }

            catch (Exception ex)
            {
                errorMessageLabel.Text = ex.ToString();
            }

            //passwordTextBox.Text = "";



        }

        protected void createDailySummaryButton_Click(object sender, EventArgs e)
        {


            Session["dateOfDailySummary"] = Convert.ToString(dateOfUnCreatedDailySummaryTextBox.Text);
            Response.BufferOutput = true;
            Response.Redirect("CreateNewDailySummary.aspx", false);

        }

        protected void ViewDetailSummary(object sender, EventArgs e)
        {

            String DetailDailySummaryDate = dailySummaryGridView.SelectedRow.Cells[1].Text;

            Session["dateOfDetailDailySummary"] = DetailDailySummaryDate;
            Response.BufferOutput = true;
            Response.Redirect("ViewDetailDailySummary.aspx", false);

        }
    }
}