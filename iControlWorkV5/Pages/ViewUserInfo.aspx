﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="ViewUserInfo.aspx.cs" Inherits="iControlWorkV5.Pages.ViewUserInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style1">
        <tr>
            <td>
                <asp:Label ID="Label" runat="server" Text="Badge No"></asp:Label>
            </td>
            <td>
                <asp:Label ID="badgeNoLabel" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="First Name"></asp:Label>
            </td>
            <td>
                <asp:Label ID="firstNameLabel17" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Middle Initial"></asp:Label>
            </td>
            <td>
                <asp:Label ID="middleIniitialLabel" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Last Name"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LastNameLabel19" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Direct No"></asp:Label>
            </td>
            <td>
                <asp:Label ID="directNoLabel" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Office Number"></asp:Label>
            </td>
            <td>
                <asp:Label ID="officeNoLabel6" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Mobile No 1"></asp:Label>
            </td>
            <td>
                <asp:Label ID="mobileNo1Label" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label8" runat="server" Text="Mobile No 2"></asp:Label>
            </td>
            <td>
                <asp:Label ID="mobileNo2Label" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label9" runat="server" Text="Department"></asp:Label>
            </td>
            <td>
                <asp:Label ID="departmentNameLabel24" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label10" runat="server" Text="Division"></asp:Label>
            </td>
            <td>
                <asp:Label ID="divisionName" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label11" runat="server" Text="Section"></asp:Label>
            </td>
            <td>
                <asp:Label ID="sectionNameLabel26" runat="server" Text=""></asp:Label>
            </td>
        </tr>
       
        <tr>
            <td>
                <asp:Label ID="Label13" runat="server" Text="Active"></asp:Label>
            </td>
            <td>
                <asp:Label ID="activeLabel28" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label14" runat="server" Text="Role"></asp:Label>
            </td>
            <td>
                <asp:Label ID="roleNameLabel12" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
<td>

    <asp:Button ID="UpdateButton" runat="server" Text="Update" OnClick="UpdateButton_Click" />
</td>

        </tr>
       
    </table>
</asp:Content>
