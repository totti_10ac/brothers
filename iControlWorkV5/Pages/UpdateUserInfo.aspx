﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="UpdateUserInfo.aspx.cs" Inherits="iControlWorkV5.Pages.UpdateUserInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
   
<div> 
        <p>Hello</p>
        <asp:Label ID="userlabel" Text="No User" runat="server" />
        <asp:Label ID="badgeNolabel" Text="No User" runat="server" />
 <%--   <p><asp:Button ID ="loggoutButton" Text ="Logout" runat ="server" OnClick="LogoutEventMethod" /></p>--%>

</div>
    <div style="background-color: #CCFFFF">

        <h4>Employee Information for Update</h4>
        <hr style="color: #808080" />
        <p>Badge Number</p>
        <asp:TextBox ID="badgeNoTextBox" Text ="" runat ="server" />
        
        <p>First Name</p>
        <asp:TextBox ID="firstnameTextBox" Text ="" runat ="server" />
        
        <p>Middle Initial</p>
        <asp:TextBox ID="middlenameTextBox" Text ="" runat ="server" />
        
        <p>Last Name</p>
        <asp:TextBox ID="lastnameTextBox" Text ="" runat ="server" />
       
        <p>Password</p>
        <asp:TextBox ID="passwordTextBox" Text ="" runat ="server" />
              
        <h4>Contact Information</h4>
        <hr style="color: #808080" />
        
        <p>Direct No</p>
        <asp:TextBox ID="directNoTextBox" Text ="" runat ="server" />

        <p> Enter phone number</p>
        <asp:TextBox ID="phonenumberTextBox" Text ="" runat ="server" />
                  
        <p>Department No</p>
        <p>
            <asp:DropDownList ID="departmentDropDownList" runat="server" OnSelectedIndexChanged="DepartmentList_SelectedIndexChanged" AutoPostBack="True">
            <asp:ListItem Text = "--Select Department--" Value = ""></asp:ListItem>
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="Select departmentName from Department where departmentName=@departmentName"></asp:SqlDataSource>
        </p>
               
        <p>Division No</p>
        <asp:DropDownList ID="divisionDropDownList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="divisionDropDownList_SelectedIndexChanged">
        <asp:ListItem Text = "--Select Division--" Value = ""></asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <p>Section No</p>
        <asp:DropDownList ID="sectionDropDownList" runat="server" AutoPostBack="True" Enabled ="false">
        <asp:ListItem Text = "--Select Section--" Value = ""></asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <h4>User Settings</h4>
        <hr style="color: #808080" />

         <p>Role of the User</p>

        <br />
        <asp:DropDownList ID="roleDropDownList1" runat="server" >
            <asp:ListItem Value="1">Admin</asp:ListItem>
            <asp:ListItem Value="1">Shift In Charge</asp:ListItem>
            <asp:ListItem Value="3">Control Engineer</asp:ListItem>
            <asp:ListItem Value="4">Other</asp:ListItem>
        </asp:DropDownList>

        <br />
      

        <p>Status</p>



        <br />
        <asp:DropDownList ID="statusDropDownList2" runat="server"  >
            <asp:ListItem Value="0">Not Active</asp:ListItem>
            <asp:ListItem Value="1">Active</asp:ListItem>
        </asp:DropDownList>



        



       <p><asp:Button ID ="updateButton" Text="Update" runat ="server" OnClick="UpdateEventMethod" /></p>
        <p><asp:Button ID ="clearButton" Text="Clear" runat ="server" OnClick="ClearEventMethod" Width="99px" />
          <p>  <asp:Label ID="messageLabel" runat="server" Text=""></asp:Label></p>
        </p>
        <p>&nbsp;</p>


    </div>
</asp:Content>
