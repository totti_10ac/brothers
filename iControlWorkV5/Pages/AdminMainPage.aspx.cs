﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iControlWorkV5.Pages
{
    public partial class AdminMainPage : System.Web.UI.Page
    {

        String userName = String.Empty;
        String badgeNumber = String.Empty;
        String roleNo = String.Empty;
        int roleNo_Int = 0;


        protected void Page_Load(object sender, EventArgs e)
        {
            userName = (String)(Session["uname"]);
            badgeNumber = (String)(Session["badgeNumber"]);
            roleNo = (String)(Session["roleNumber"]);
            if (roleNo == null)
            {
                roleNo = "0";
            
            }
            roleNo_Int = Int16.Parse(roleNo);
            if (userName == null || roleNo_Int != 1)
            {
                Response.BufferOutput = true;
                Response.Redirect("../Default.aspx", false);

            }
            else
            {


                userlabel.Text = userName;
                badgeNolabel.Text = Convert.ToString(badgeNumber);
            }
        }
        protected void LogoutEventMethod(object sender, EventArgs e)
        {

            Session["unmae"] = null;
            Session.Abandon();
            Response.BufferOutput = true;
            Response.Redirect("../Default.aspx");

        }

        protected void ViewUserInfo(object sender, EventArgs e) 
        {

            String badgeNo = GridView1.SelectedRow.Cells[1].Text;

            Session["selectedBadgeNo"] = badgeNo;
            Response.BufferOutput = true;
            Response.Redirect("ViewUserInfo.aspx", false);

        
        
        }

     
    }
}