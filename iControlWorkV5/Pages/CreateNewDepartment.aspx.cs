﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iControlWorkV5.Classes;

namespace iControlWorkV5.Pages
{
    public partial class CreateNewDepartment : System.Web.UI.Page
    {
        String userName = String.Empty;
        String badgeNumber = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            userName = (String)(Session["uname"]);
            badgeNumber = (String)(Session["badgeNumber"]);

            if (userName == null)
            {
                Response.BufferOutput = true;
                Response.Redirect("../Default.aspx", false);

            }
            else
            {


                userLabel.Text = userName;
                badgeNoLabel.Text = Convert.ToString(badgeNumber);
            }

        }

        protected void createNewDepartmentButton_Click(object sender, EventArgs e)
        {
            String messageReturned = String.Empty;
            Department Department = new Department();
            
            messageReturned = Department.CreateDepartment(newDepartmentTextBox.Text);
         

        }

        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow && GridView1.EditIndex != e.Row.RowIndex)
            {

                (e.Row.Cells[2].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to Update this row?');";

            }

        }
    }
}