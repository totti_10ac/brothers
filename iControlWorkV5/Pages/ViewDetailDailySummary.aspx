﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="ViewDetailDailySummary.aspx.cs" Inherits="iControlWorkV5.Pages.ViewDetailDailySummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style1">
        <tr>
            <td>
                <asp:Label ID="dateOfDailySummaryLabel1" runat="server" Text=""></asp:Label>

            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSource1">
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
                    SelectCommand="select netWorkArea, timeOfTripping, restorationTimeActual, voltagelevel, circuitOrEquipment, protectionRelay, descriptionOfTripping, Comment from TrippingReport where dateOfTripping= @dateOfDetailDailySummary">
                    <SelectParameters>

                        <asp:SessionParameter DefaultValue="0" Name="dateOfDetailDailySummary" SessionField="dateOfDetailDailySummary" Type="DateTime"/>

                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
