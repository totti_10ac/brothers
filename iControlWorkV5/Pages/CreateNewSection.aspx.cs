﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iControlWorkV5.Classes;

namespace iControlWorkV5.Pages
{
    public partial class CreateNewSection : System.Web.UI.Page
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        // MySql.Data.MySqlClient.MySqlDataReader reader;
        String userName = String.Empty;
        String badgeNumber = String.Empty;



        protected void Page_Load(object sender, EventArgs e)
        {
            userName = (String)(Session["uname"]);
            badgeNumber = (String)(Session["badgeNumber"]);

            if (userName == null)
            {
                Response.BufferOutput = true;
                Response.Redirect("../Default.aspx", false);

            }
            else
            {


                userNameLabel.Text = userName;
                badgeNoLabel.Text = Convert.ToString(badgeNumber);
            }

            //Filling in the dropdown lists

            
            if (!IsPostBack)
            {

                departmentDropDownList.AppendDataBoundItems = true;
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

                String strQuery = "select departmentNo, departmentName from Department";
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                cmd = new MySql.Data.MySqlClient.MySqlCommand(strQuery, conn);
                // reader = cmd.ExecuteReader();
                try
                {
                    conn.Open();
                    departmentDropDownList.DataSource = cmd.ExecuteReader();
                    departmentDropDownList.DataTextField = "departmentName";
                    departmentDropDownList.DataValueField = "departmentNo";
                    departmentDropDownList.DataBind();
                }
                catch (Exception ex)
                {
                    userNameLabel.Text = Convert.ToString(ex);
                    throw ex;

                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
            }


        }

        protected void DepartmentSelection_SelectedIndexChanged(object sender, EventArgs e)
                {


                   
                   
                    divisionDropDownList.Items.Clear();

                    divisionDropDownList.Items.Add(new ListItem("--Select Division--", ""));

                    divisionDropDownList.AppendDataBoundItems = true;
                    string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

                    String queryStr = "select divisionNo, divisionName from Division where departmentNo=@departmentNo";
                    conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                    cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);

                    cmd.Parameters.AddWithValue("@departmentNo", departmentDropDownList.SelectedItem.Value);


               
               

          
               

               // conn.Open();

                 try
                {
                    conn.Open();
                    divisionDropDownList.DataSource = cmd.ExecuteReader();
                    divisionDropDownList.DataTextField = "divisionName";
                    divisionDropDownList.DataValueField = "divisionNo";
                    divisionDropDownList.DataBind();
                if (divisionDropDownList.Items.Count > 1)

                {

                    divisionDropDownList.Enabled = true;

                }

                else

                {

                    divisionDropDownList.Enabled = false;

                   // ddlCity.Enabled = false; 

                }

            }
           

            catch (Exception ex)

            {
                userNameLabel.Text = Convert.ToString(ex);
                //throw  ex;

            }

            finally

            {

                conn.Close();

                conn.Dispose();

            }

                }

         



        protected void createNewSectionButton_Click(object sender, EventArgs e)
        {
            try
            {
                String messageReturned = String.Empty;

                Section Section = new Section();

                int departmentNo = Convert.ToInt16(departmentDropDownList.SelectedValue);
                int divisionNo = Convert.ToInt16(divisionDropDownList.SelectedValue);

                messageReturned = Section.CreateNewSection(departmentNo, divisionNo, newSectionTextBox.Text);
                returnedMessageLabel.Text = messageReturned;
                newSectionTextBox.Text = "";
            }
            catch (Exception ex)
            {
                userNameLabel.Text = Convert.ToString(ex);
                //throw ex;

            }
           
        }


    }
}