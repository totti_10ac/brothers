﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace iControlWorkV5.Pages
{
    public partial class CreateNewDailySummary : System.Web.UI.Page
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        String queryStr;
        String dateOfDailySummary = String.Empty;
        bool correctDate ;
        String userName = String.Empty;
        String badgeNumber = String.Empty;
        DateTime dayOfSummary;

        protected void Page_Load(object sender, EventArgs e)
        {
            dateOfDailySummary = (String)(Session["dateOfDailySummary"]);
            dayOfSummary = Convert.ToDateTime(dateOfDailySummary);


            userName = (String)(Session["uname"]);
            badgeNumber = (String)(Session["badgeNumber"]);
          

            if (userName == null || (dateOfDailySummary == null))
            {
                Response.BufferOutput = true;
                Response.Redirect("../Default.aspx", false);

            }
         
         
            else if (dayOfSummary >= (DateTime.Now))
                {
                    dateOfDailySummaryLabel.Text = "Sorry it is not possible to create the daily summary!";
                    correctDate = false;

                    Response.BufferOutput = true;
                    Response.Redirect("ViewDailySummary.aspx", false);
                
                    
                }
           else
                { 
                userNameLabel.Text = userName;
                badgeNoLabel.Text = Convert.ToString(badgeNumber);
                dateOfDailySummaryLabel.Text = dayOfSummary.ToShortDateString();
                correctDate = true;
                }
            }

           

       

 

        protected void createDailySummaryButton_Click(object sender, EventArgs e)
        {
           //Defining the variables
            
            String dateOfSummary;
           // DateTime timeOfSummary1;
            String timeOfSummary;
            decimal maxTemperature;
            decimal minTemperature;
            decimal maxPowerDemand;
            decimal minPowerDemand;
            String fullNameOfFirstEngineer = String.Empty;
            String fullNameOfSecondEngineer = String.Empty;
            String fullNameofSectionHead = String.Empty;
            String fullNameOfDivisionManager = String.Empty;
            String fullNameOfDepartmentManager = String.Empty;

            //Assigning variables' values
            if (correctDate == true) { 
            try {

                dateOfSummary = string.Format("{0:yyyy-MM-dd}", dayOfSummary);
              //  String dateOfSummaryString = dateOfSummary.ToString("yyyy-mm-dd"); 
           
           // timeOfSummary1= DateTime.Now;
            timeOfSummary = string.Format("{0:HH:mm:ss}", DateTime.Now);
            maxTemperature = Convert.ToDecimal(maxTempTextBox.Text);
            minTemperature = Convert.ToDecimal(minTempTextBox.Text);
            maxPowerDemand = Convert.ToDecimal(maxDemandTextBox.Text);
            minPowerDemand = Convert.ToDecimal(minDemandTextBox.Text);
            fullNameOfFirstEngineer = firstEngineerNameTextBox.Text;
            fullNameOfSecondEngineer = secondEngineerNameTextBox.Text;
            fullNameofSectionHead = null;
            fullNameOfDivisionManager = null;
            fullNameOfDepartmentManager= null;
            

             
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            conn.Open();
            queryStr = "INSERT INTO Turkif.DailySummary"+
                        "(  dateOfSummary           ,   timeOfSummary,    maxTemperature, minTemperature, maxPowerDemand, minPowerDemand," + 
                        "   fullNameOfFirstEngineer ,   fullNameOfSecondEngineer,   sectionHeadFullName,    divisionManagerFullName,"+
                        "departmentManagerFullName)" +
                        "Values "+
                        "('" + dateOfSummary + "','" + timeOfSummary + "','" + maxTemperature + "','" + minTemperature + "','" + maxPowerDemand + "','" +
                           minPowerDemand + "','" + fullNameOfFirstEngineer + "','" + fullNameOfSecondEngineer + "','" + fullNameofSectionHead + "','" + 
                           fullNameOfDivisionManager + "','"+fullNameOfDepartmentManager+"')";

            
            cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
           
            
            cmd.ExecuteReader();
            conn.Close();
            }
           
            catch (Exception ex)
            {

                throw;

            }
            }

            Response.Redirect("ViewDailySummary.aspx", false);
           
        }
     }
}