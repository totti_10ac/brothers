﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace iControlWorkV5.Pages
{
    public partial class CreateNewTrippingReport : System.Web.UI.Page
    {

        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        String queryStr;
        String userName = String.Empty;
        String badgeNumber = String.Empty;
        String messageOfSucessRecieved = String.Empty;



        protected void Page_Load(object sender, EventArgs e)
        {
            //setting the time text box with the time now
            //trippingReportDateTextBox.Text = DateTime.Today.ToString("dd/mm/yyyy");
            //trippingTimeTextBox.Text = DateTime.Now.ToString();;
            userName                        = (String)(Session["uname"]);
            badgeNumber                     = (String)(Session["badgeNumber"]);
            messageOfSucessRecieved         = (String)(Session["messageOfSucess"]);
            errorMessageLabel.Text          = messageOfSucessRecieved;
            trippingReportDateTextBox.Text  = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            trippingTimeTextBox.Text        = string.Format("{0:HH:mm:ss}", DateTime.Now);

            if (userName == null)
            {
                Response.BufferOutput = true;
                Response.Redirect("../Default.aspx", false);
            }

            else
            {
                int count = 0;
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();
                queryStr = "SELECT count(*) FROM Turkif.TrippingReport;";
                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
                count = int.Parse(cmd.ExecuteScalar().ToString());
                count = count + 1;
                TrippingReportLabel.Text = Convert.ToString(count);
                UserNameLabel.Text = userName;
            }
        }

        /*
         * 
         */
        protected void ActualResturationTimeTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        protected void CreateTrippingReport(object sender, EventArgs e)
        {
            DateTime trippingDate1;
            DateTime trippingResotreExpexted;
            DateTime trippingResotreActual;
            String trippingDate = String.Empty;
            String trippingTime = String.Empty;
            String causeOfTripping = String.Empty;
            String actualRestorationTime = String.Empty;
            String expectedRestorationTime = String.Empty;
            String actualRestorationDate = String.Empty;
            String expectedRestorationDate = String.Empty;
            String networkArea = String.Empty;
            String votageLevel = String.Empty;
            String trippingDescription = String.Empty;
            String substation = String.Empty;
            String ciricutEquipment = String.Empty;
            String lostSupplyArea = String.Empty;
            String powerLostVolume = String.Empty;
            String protectionRelay = String.Empty;
            String comment = String.Empty;
            int trippingReportNumber = 0;
            int badgeNumberInt = 0;
            int numberOfEffectedConsumers = 0;


            try
            {

                trippingDate1 = Convert.ToDateTime(trippingReportDateTextBox.Text);
                trippingResotreExpexted = Convert.ToDateTime(expectedRestorationDateTextBox.Text);
                trippingResotreActual = Convert.ToDateTime(actualRestorationDateTextBox.Text);

                trippingReportNumber = int.Parse(TrippingReportLabel.Text);
                badgeNumberInt = int.Parse(badgeNumber);
                trippingDate = string.Format("{0:yyyy-MM-dd}", trippingDate1);
                trippingTime = string.Format("{0:HH:mm:ss}", trippingTimeTextBox.Text);
                causeOfTripping = causeOfTrippingTextBox.Text;

                actualRestorationTime = actualRestorationTimeTextBox.Text;
                expectedRestorationTime = expectedResotrationTimeTextBox.Text;

                actualRestorationDate = string.Format("{0:yyyy-MM-dd}", trippingResotreActual);
                expectedRestorationDate = string.Format("{0:yyyy-MM-dd}", trippingResotreExpexted);

                networkArea = Convert.ToString(NetworkAreaDropDownList.SelectedItem);
                votageLevel = Convert.ToString(VoltageLevelDropDownList.SelectedItem);
                trippingDescription = trippingDescriptionTextBox.Text;
                substation = Convert.ToString(SubstationDropDowmList.SelectedItem);
                ciricutEquipment = Convert.ToString(CircuitEquipmentDropDownList.SelectedItem);
                lostSupplyArea = Convert.ToString(LostSupplyAreaDropDownList.SelectedItem);
                powerLostVolume = Convert.ToString(PowerLostVolumeTextBox.Text);
                numberOfEffectedConsumers = int.Parse(NumberOfEffectedConsumersTextBox.Text);
                protectionRelay = ProtectionRelayTextBox0.Text;
                comment = commentTextBox.Text;

                String connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();
                queryStr = "INSERT INTO Turkif.TrippingReport" +
                            "   (trippingReportNo               ,   badgeNo                         ,   dateOfTripping              ,   " +
                            "   timeOfTripping                  ,   causeOfTripping                 ,   restorationTimeActual       ,   " +
                            "   restorationTimeExpected         ,   restorationDateActual           ,   restorationDateExpected     ,   " +
                            "   netWorkArea                     ,   voltagelevel                    ,   descriptionOfTripping       ,   " +
                            "   substation                      ,   circuitOrEquipment              ,   areaOfLostSupply            ,   " +
                            "   volumOfPowerLost                ,   noOfConsumerEffected            ,   protectionRelay             ,   " +
                            "   comment                        )" +
                            "   Values                          " +
                            "   (                               '" +
                                trippingReportNumber + "','" + badgeNumberInt + "','" + trippingDate + "','" +
                                trippingTime + "','" + causeOfTripping + "','" + actualRestorationTime + "','" +
                                expectedRestorationTime + "','" + actualRestorationDate + "','" + expectedRestorationDate + "','" +
                                networkArea + "','" + votageLevel + "','" + trippingDescription + "','" +
                                substation + "','" + ciricutEquipment + "','" + lostSupplyArea + "','" +
                                powerLostVolume + "','" + numberOfEffectedConsumers + "','" + protectionRelay + "','" +
                                comment + "') ";


                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
                cmd.ExecuteReader();
                conn.Close();

            }

            catch (Exception ex)
            {
                errorMessageLabel.Text = ex.ToString();
            }
            Session["messageOfSucess"] = "Record has been successfully created";
            Response.BufferOutput = true;
            Response.Redirect("CreateNewTrippingReport.aspx", false);
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
           
        }











    }
}