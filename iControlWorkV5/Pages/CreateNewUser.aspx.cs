﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iControlWorkV5.Pages
{
    public partial class CreateNewUser : System.Web.UI.Page
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        String userName = String.Empty;
        String badgeNumber = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            userName = (String)(Session["uname"]);
            badgeNumber = (String)(Session["badgeNumber"]);

            if (userName == null)
            {
                Response.BufferOutput = true;
                Response.Redirect("../Default.aspx", false);

            }
            else
            {


                userlabel.Text = userName;
                badgeNolabel.Text = Convert.ToString(badgeNumber);
            }

            //Filling in the dropdown lists

            if (!IsPostBack)
            {
                departmentDropDownList.AppendDataBoundItems = true;
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
                
                String strQuery = "select departmentNo, departmentName from Department";
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                cmd = new MySql.Data.MySqlClient.MySqlCommand(strQuery, conn);
             
                try
                {
                    conn.Open();
                    departmentDropDownList.DataSource = cmd.ExecuteReader();
                    departmentDropDownList.DataTextField = "departmentName";
                    departmentDropDownList.DataValueField = "departmentNo";
                    departmentDropDownList.DataBind();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

        }

        protected void RegisterEventMethod(object sender, EventArgs e)
        {
            User User = new User();

            messageLabel.Text = User.CreateNewUser(badgeNoTextBox.Text, firstnameTextBox.Text, middlenameTextBox.Text, lastnameTextBox.Text, 
                                                    passwordTextBox.Text, statusDropDownList2.SelectedValue, directNoTextBox.Text, 
                                                    phonenumberTextBox.Text, roleDropDownList1.SelectedValue,departmentDropDownList.SelectedValue, 
                                                    divisionDropDownList.SelectedValue, sectionDropDownList.SelectedValue, MobileNo1TextBox.Text, 
                                                    MobileNo2TextBox.Text);
            Response.BufferOutput = true;
            Response.Redirect("AdminMainPage.aspx");
        }

        protected void LogoutEventMethod(object sender, EventArgs e)
        {

            Session["unmae"] = null;
            Session.Abandon();
                Response.BufferOutput = true;
                Response.Redirect("../Default.aspx");

        }

        protected void ClearEventMethod(object sender, EventArgs e)
        {
            badgeNoTextBox.Text = "";
            firstnameTextBox.Text = "";
            middlenameTextBox.Text = "";
            lastnameTextBox.Text = "";
            passwordTextBox.Text = "";
            directNoTextBox.Text = "";
            phonenumberTextBox.Text = "";
            divisionDropDownList.Items.Clear();
            divisionDropDownList.Items.Add(new ListItem("--Select Division--", ""));
            sectionDropDownList.Items.Clear();
            sectionDropDownList.Items.Add(new ListItem("--Select Section--", ""));

        }

        protected void DepartmentList_SelectedIndexChanged(object sender, EventArgs e)
        {
            divisionDropDownList.Items.Clear();
            divisionDropDownList.Items.Add(new ListItem("--Select Division--", ""));
            sectionDropDownList.Items.Clear();
            sectionDropDownList.Items.Add(new ListItem("--Select Section--", ""));

            divisionDropDownList.AppendDataBoundItems = true;
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            String strQuery = "select divisionNo, divisionName from Division where departmentNo=@departmentNo";
            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            cmd = new MySql.Data.MySqlClient.MySqlCommand(strQuery, conn);

         
            cmd.Parameters.AddWithValue("@departmentNo", departmentDropDownList.SelectedItem.Value);
         
            try
            {
                conn.Open();
                divisionDropDownList.DataSource = cmd.ExecuteReader();
                divisionDropDownList.DataTextField = "divisionName";
                divisionDropDownList.DataValueField = "divisionNo";
                divisionDropDownList.DataBind();
                if (divisionDropDownList.Items.Count > 1)
                {
                    divisionDropDownList.Enabled = true;
                }
                else
                {
                    divisionDropDownList.Enabled = false;
                    sectionDropDownList.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        protected void divisionDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            sectionDropDownList.Items.Clear();
            sectionDropDownList.Items.Add(new ListItem("--Select Section--", ""));
            sectionDropDownList.AppendDataBoundItems = true;
           
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
            String strQuery = "select sectionNo, sectionName from Section where divisionNo=@divisionNo";
            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            cmd = new MySql.Data.MySqlClient.MySqlCommand(strQuery, conn);
            cmd.Parameters.AddWithValue("@divisionNo",divisionDropDownList.SelectedItem.Value);
            try
            {
                conn.Open();
                sectionDropDownList.DataSource = cmd.ExecuteReader();
                sectionDropDownList.DataTextField = "sectionName";
                sectionDropDownList.DataValueField = "SectionNo";
                sectionDropDownList.DataBind();
                if (sectionDropDownList.Items.Count > 1)
                {
                    sectionDropDownList.Enabled = true;
                }
                else
                {
                    sectionDropDownList.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }
    }
}