﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="ViewDailySummary.aspx.cs" Inherits="iControlWorkV5.Pages.ViewDailySummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
            <style type="text/css">
                .auto-style1 {
                    width: 100%;
                }
            </style>
            </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>



        <table class="auto-style1">
            <tr>
                <td>
                    <asp:Label ID="userNameLabel" runat="server" Text="User Name"></asp:Label>

                    <asp:Label ID="badgeNoLabel" runat="server" Text="Badge No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="errorMessageLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="dailySummaryGridView" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="dateOfSummary" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="ViewDetailSummary">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" />
                            <asp:BoundField DataField="dateOfSummary" HeaderText="Date of Summary" ReadOnly="True" SortExpression="dateOfSummary" />
                            <asp:BoundField DataField="timeOfSummary" HeaderText="Time of Summary" SortExpression="timeOfSummary" />
                        </Columns>
                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>

                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                        ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
                        SelectCommand="Select dateOfSummary, timeOfSummary from DailySummary order by dateOfSummary desc;">
                    </asp:SqlDataSource>

                </td>

                <td>
                    <p>
                        <asp:Label ID="createNewDailySummaryLabel" runat="server" Text="Create Daily Summary for the following date"></asp:Label></p>
                    <p>
                        <asp:TextBox ID="dateOfUnCreatedDailySummaryTextBox" runat="server"></asp:TextBox></p>
                    <p>
                        <asp:Button ID="createDailySummaryButton" runat="server" Text="Create Daily Summary" OnClick="createDailySummaryButton_Click" /></p>
                </td>
            </tr>
        </table>



    </div>
</asp:Content>
