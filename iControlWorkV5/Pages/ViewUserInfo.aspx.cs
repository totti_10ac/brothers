﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iControlWorkV5.Pages
{
    public partial class ViewUserInfo : System.Web.UI.Page
    {
        String badgeNumber = String.Empty;
        String userName = String.Empty;
        String badgeNo = String.Empty;

        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        String queryStr;

        protected void Page_Load(object sender, EventArgs e)
        {
            userName = (String)(Session["uname"]);
            badgeNumber = (String)(Session["badgeNumber"]);

            if (userName == null)
            {
                Response.BufferOutput = true;
                Response.Redirect("../Default.aspx", false);

            }

            else
            {

                badgeNo = (String)(Session["selectedBadgeNo"]);
                int badgeNoin = Int32.Parse(badgeNo);

            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();

                queryStr = "select * from  User,Department, Division, Section, Role where User.badgeNo = '" + badgeNoin + "' and Department.departmentNo=User.departmentNo and Division.divisionNo=User.divisionNo and Section.sectionNo=User.SectionNo  and Role.roleNo=User.roleNo ";

                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);

                reader = cmd.ExecuteReader();

                while (reader.HasRows && reader.Read())
                {
                    badgeNoLabel.Text = reader.GetString(reader.GetOrdinal("badgeNo"));
                    firstNameLabel17.Text = reader.GetString(reader.GetOrdinal("firstName"));
                    middleIniitialLabel.Text = reader.GetString(reader.GetOrdinal("middleInitial"));
                    LastNameLabel19.Text = reader.GetString(reader.GetOrdinal("lastName"));
                    directNoLabel.Text = reader.GetString(reader.GetOrdinal("directNumber"));
                    officeNoLabel6.Text = reader.GetString(reader.GetOrdinal("officeNumber"));
                    departmentNameLabel24.Text = reader.GetString(reader.GetOrdinal("departmentName"));
                    divisionName.Text = reader.GetString(reader.GetOrdinal("divisionName"));
                    sectionNameLabel26.Text = reader.GetString(reader.GetOrdinal("sectionName"));
                    roleNameLabel12.Text = reader.GetString(reader.GetOrdinal("roleName"));
                    activeLabel28.Text = reader.GetString(reader.GetOrdinal("active"));
                    
                    
                }
            }

            catch (Exception ex)
            {

                throw;
            }
            }
        }

        protected void UpdateButton_Click(object sender, EventArgs e)
        {
            
            Session["selectedBadgeNo"] = badgeNo;
            Response.BufferOutput = true;
            Response.Redirect("UpdateUserInfo.aspx", false);
        }
    }
}