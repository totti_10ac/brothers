﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/ControlEngineer.Master" AutoEventWireup="true" CodeBehind="CreateNewTrippingReport.aspx.cs" Inherits="iControlWorkV5.Pages.CreateNewTrippingReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style10" colspan="2">
                    <asp:Label ID="UserNameLabel" runat="server" Text="UserNameLabel"></asp:Label>
                </td>
                <td class="auto-style4" colspan="2"></td>
            </tr>
           
            <tr>
                <td class="auto-style4" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><span class="auto-style7">Adding Tripping Report Page</span></strong>&nbsp;</td>
            </tr>
           
            <tr>
                <td class="auto-style9" colspan="4">
                    <asp:Label ID="errorMessageLabel" runat="server" ForeColor="#33CC33"></asp:Label>
                </td>
            </tr>
           
            <tr>
                <td class="auto-style24"></td>
                <td class="auto-style11" colspan="2">Tripping Report Number </td>
                <td class="auto-style21">
                    <asp:Label ID="TrippingReportLabel" runat="server" Text="TrippingReportLabel"></asp:Label>
                </td>
            </tr>
           
            <tr>
                <td class="auto-style17"></td>
                <td class="auto-style18" colspan="2">Tripping Date </td>
                <td class="auto-style19">
                    <br />
                    <asp:TextBox ID="trippingReportDateTextBox" runat="server" TextMode="Date" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">Tripping Time </td>
                <td class="auto-style23">
                    <asp:TextBox ID="trippingTimeTextBox" runat="server" TextMode="Time"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style14"></td>
                <td class="auto-style15" colspan="2">Cause of Tripping </td>
                <td class="auto-style16">
                    <asp:TextBox ID="causeOfTrippingTextBox" runat="server" Height="68px" TextMode="MultiLine" Width="451px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">Actual Restoration Time </td>
                <td class="auto-style23">
                    <asp:TextBox ID="actualRestorationTimeTextBox" runat="server" TextMode="Time">23:00:00</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">Expected Restoration Time </td>
                <td class="auto-style23">
                    <asp:TextBox ID="expectedResotrationTimeTextBox" runat="server" TextMode="Time">23:00:00</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">Actual Restoration Date</td>
                <td class="auto-style23">
                    <asp:TextBox ID="actualRestorationDateTextBox" runat="server" TextMode="Date">31-12-2015</asp:TextBox>
                    <br />
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">Expected Restoration Date</td>
                <td class="auto-style23">
                    <asp:TextBox ID="expectedRestorationDateTextBox" runat="server" TextMode="Date">31-12-2015</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">Network Area </td>
                <td class="auto-style23">
                    <asp:DropDownList ID="NetworkAreaDropDownList" runat="server" Height="17px" Width="375px">
                        <asp:ListItem>Riyadh</asp:ListItem>
                        <asp:ListItem>Kharj</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">Voltage Level </td>
                <td class="auto-style23">
                    <asp:DropDownList ID="VoltageLevelDropDownList" runat="server" Height="17px" Width="375px">
                        <asp:ListItem Value="33">33</asp:ListItem>
                        <asp:ListItem Value="13.8">13.8</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">Tripping Description </td>
                <td class="auto-style23">
                    <asp:TextBox ID="trippingDescriptionTextBox" runat="server" Height="79px" TextMode="MultiLine" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style24"></td>
                <td class="auto-style11" colspan="2">Substation </td>
                <td class="auto-style21">
                    <asp:DropDownList ID="SubstationDropDowmList" runat="server" Height="17px" Width="375px">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">Circuit Equipment </td>
                <td class="auto-style23">
                    <asp:DropDownList ID="CircuitEquipmentDropDownList" runat="server" Height="17px" Width="375px">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">Lost Supply Area </td>
                <td class="auto-style23">
                    <asp:DropDownList ID="LostSupplyAreaDropDownList" runat="server" Height="17px" Width="375px">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style24"></td>
                <td class="auto-style11" colspan="2">Power Lost Volume </td>
                <td class="auto-style21">
                    <asp:TextBox ID="PowerLostVolumeTextBox" runat="server" Width="357px" CausesValidation="True" Height="17px">0</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">Number of Effected Consumers </td>
                <td class="auto-style23">
                    <asp:TextBox ID="NumberOfEffectedConsumersTextBox" runat="server" Width="357px" CausesValidation="True" Height="17px">0</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">Protection Relay </td>
                <td class="auto-style23">
                    <asp:TextBox ID="ProtectionRelayTextBox0" runat="server" Width="357px" CausesValidation="True" Height="17px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">Comment </td>
                <td class="auto-style23">
                    <asp:TextBox ID="commentTextBox" runat="server" Height="52px" TextMode="MultiLine" Width="388px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">&nbsp;</td>
                <td class="auto-style3" colspan="2">&nbsp;</td>
                <td class="auto-style23">
                    <asp:Button ID="CreateTrippingButton" runat="server" OnClick="CreateTrippingReport" Text="Create" Width="145px" ValidationGroup="1" />
                </td>
            </tr>
        </table>
    
    </div>
        
</asp:Content>
