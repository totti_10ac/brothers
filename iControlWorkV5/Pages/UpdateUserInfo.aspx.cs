﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iControlWorkV5.Pages
{
    public partial class UpdateUserInfo : System.Web.UI.Page
    {
        String badgeNumber = String.Empty;

        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        String queryStr;

        protected void Page_Load(object sender, EventArgs e)
        {
            badgeNumber = (String)(Session["badgeNumber"]);

            if (badgeNumber == null)
            {
                Response.BufferOutput = true;
                Response.Redirect("../Default.aspx", false);

            }

            else
            {
                if (!IsPostBack)
                {


                    badgeNumber = (String)(Session["selectedBadgeNo"]);
                    int badgeNoin = Int32.Parse(badgeNumber);

                    try
                    {
                        string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

                        conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                        conn.Open();

                        queryStr = "select * from  User,Department, Division, Section, Role where User.badgeNo = '" + badgeNoin + "' and Department.departmentNo=User.departmentNo and Division.divisionNo=User.divisionNo and Section.sectionNo=User.SectionNo  and Role.roleNo=User.roleNo ";

                        cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);

                        reader = cmd.ExecuteReader();

                        while (reader.HasRows && reader.Read())
                        {

                            badgeNoTextBox.Text = reader.GetString(reader.GetOrdinal("badgeNo"));
                            firstnameTextBox.Text = reader.GetString(reader.GetOrdinal("firstName"));
                            middlenameTextBox.Text = reader.GetString(reader.GetOrdinal("middleInitial"));
                            lastnameTextBox.Text = reader.GetString(reader.GetOrdinal("lastName"));
                            directNoTextBox.Text = reader.GetString(reader.GetOrdinal("directNumber"));
                            phonenumberTextBox.Text = reader.GetString(reader.GetOrdinal("officeNumber"));
                            roleDropDownList1.Text = reader.GetString(reader.GetOrdinal("roleName"));
                            statusDropDownList2.Text = reader.GetString(reader.GetOrdinal("active"));
                          //  departmentDropDownList.DataTextField = reader.GetString(reader.GetOrdinal("departmentName"));

                        }



                    }

                    catch (Exception ex)
                    {
                        throw;
                    }
                }
                if (!IsPostBack)
                {
                    departmentDropDownList.AppendDataBoundItems = true;
                    string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

                    String strQuery = "select departmentNo, departmentName from Department";
                    conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                    cmd = new MySql.Data.MySqlClient.MySqlCommand(strQuery, conn);

                    try
                    {
                        conn.Open();
                        departmentDropDownList.DataSource = cmd.ExecuteReader();
                        departmentDropDownList.DataTextField = "departmentName";
                        departmentDropDownList.DataValueField = "departmentNo";
                        departmentDropDownList.DataBind();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }

        protected void DepartmentList_SelectedIndexChanged(object sender, EventArgs e)
        {
            divisionDropDownList.Items.Clear();
            divisionDropDownList.Items.Add(new ListItem("--Select Division--", ""));
            sectionDropDownList.Items.Clear();
            sectionDropDownList.Items.Add(new ListItem("--Select Section--", ""));

            divisionDropDownList.AppendDataBoundItems = true;
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            String strQuery = "select divisionNo, divisionName from Division where departmentNo=@departmentNo";
            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            cmd = new MySql.Data.MySqlClient.MySqlCommand(strQuery, conn);
            cmd.Parameters.AddWithValue("@departmentNo", departmentDropDownList.SelectedItem.Value);
            try
            {
                conn.Open();
                divisionDropDownList.DataSource = cmd.ExecuteReader();
                divisionDropDownList.DataTextField = "divisionName";
                divisionDropDownList.DataValueField = "divisionNo";
                divisionDropDownList.DataBind();
                if (divisionDropDownList.Items.Count > 1)
                {
                    divisionDropDownList.Enabled = true;
                }
                else
                {
                    divisionDropDownList.Enabled = false;
                    sectionDropDownList.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        protected void divisionDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            sectionDropDownList.Items.Clear();
            sectionDropDownList.Items.Add(new ListItem("--Select Section--", ""));
            sectionDropDownList.AppendDataBoundItems = true;

            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
            String strQuery = "select sectionNo, sectionName from Section where divisionNo=@divisionNo";
            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            cmd = new MySql.Data.MySqlClient.MySqlCommand(strQuery, conn);
            cmd.Parameters.AddWithValue("@divisionNo", divisionDropDownList.SelectedItem.Value);
            try
            {
                conn.Open();
                sectionDropDownList.DataSource = cmd.ExecuteReader();
                sectionDropDownList.DataTextField = "sectionName";
                sectionDropDownList.DataValueField = "SectionNo";
                sectionDropDownList.DataBind();
                if (sectionDropDownList.Items.Count > 1)
                {
                    sectionDropDownList.Enabled = true;
                }
                else
                {
                    sectionDropDownList.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        protected void UpdateEventMethod(object sender, EventArgs e)
        {
            String firstName = String.Empty;
            firstName = firstnameTextBox.Text;

            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            conn.Open();
            queryStr = "Update User Set " +
                        "badgeNo = '" + badgeNoTextBox.Text + "', firstName = '" + firstnameTextBox.Text + "', middleInitial = '"+middlenameTextBox.Text+
                         "',lastName = '"+lastnameTextBox.Text+"',directNumber ='"+directNoTextBox.Text+"',officeNumber = '"+phonenumberTextBox.Text+
                         "',password = '"+passwordTextBox.Text+
                        // "', departmentNo ='"+departmentDropDownList.SelectedValue+"',divisionNo = '"+divisionDropDownList.SelectedValue+"',sectionNo='"+sectionDropDownList.SelectedValue+
                         "', roleNo = '"+roleDropDownList1.SelectedValue+"', active = '"+statusDropDownList2.SelectedValue+
                         "' where badgeNo ='" + badgeNoTextBox.Text + "'"; 


            cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
            cmd.ExecuteReader();
            conn.Close();
            

        }

        protected void ClearEventMethod(object sender, EventArgs e)
        {



        }
    }
}