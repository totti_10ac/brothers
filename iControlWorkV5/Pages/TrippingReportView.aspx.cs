﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iControlWorkV5.Pages
{
    public partial class TrippingReportView: System.Web.UI.Page
    {
     MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        String queryStr;
        String userName = String.Empty;
        String badgeNumber = String.Empty;
        String reportNo = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                

            userName = (String)(Session["uname"]);
            badgeNumber = (String)(Session["badgeNumber"]);
            reportNo = (String)(Session["reportNo"]);
            
            if (userName == null)
            {
                Response.BufferOutput = true;
                Response.Redirect("../Default.aspx", false);

            }
            else
            {
                UserNameLabel.Text = userName;
                badgeNoLabel.Text = badgeNumber;
               
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();

                
           

                queryStr = "select * from Turkif.TrippingReport where trippingReportNo = '" + reportNo + "'";

               cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);

                reader = cmd.ExecuteReader();


                while (reader.HasRows && reader.Read())
               {
                   TrippingReportLabel.Text                     = reader.GetString(reader.GetOrdinal("trippingReportNo"));
                   trippingReportDateTextBox.Text               = reader.GetString(reader.GetOrdinal("dateOfTripping"));
                   trippingTimeTextBox.Text                     = reader.GetString(reader.GetOrdinal("timeOfTripping"));
                   causeOfTrippingTextBox.Text                  = reader.GetString(reader.GetOrdinal("causeOfTripping"));
                   actualRestorationTimeTextBox.Text            = reader.GetString(reader.GetOrdinal("restorationTimeActual"));
                   expectedResotrationTimeTextBox.Text          = reader.GetString(reader.GetOrdinal("restorationTimeExpected"));
                   actualRestorationDateTextBox.Text            = reader.GetString(reader.GetOrdinal("restorationDateActual"));
                   expectedRestorationDateTextBox.Text          = reader.GetString(reader.GetOrdinal("restorationDateExpected"));
                   NetworkAreaDropDownList.SelectedValue        = reader.GetString(reader.GetOrdinal("netWorkArea"));
                   VoltageLevelDropDownList.SelectedValue       = reader.GetString(reader.GetOrdinal("voltagelevel"));
                   trippingDescriptionTextBox.Text              = reader.GetString(reader.GetOrdinal("descriptionOfTripping"));
                   CircuitEquipmentDropDownList.SelectedValue   = reader.GetString(reader.GetOrdinal("circuitOrEquipment"));
                   LostSupplyAreaDropDownList.SelectedValue     = reader.GetString(reader.GetOrdinal("areaOfLostSupply"));
                   PowerLostVolumeTextBox.Text                  = reader.GetString(reader.GetOrdinal("volumOfPowerLost"));
                   NumberOfEffectedConsumersTextBox.Text        = reader.GetString(reader.GetOrdinal("noOfConsumerEffected"));
                   ProtectionRelayTextBox0.Text                 = reader.GetString(reader.GetOrdinal("protectionRelay"));
                   commentTextBox.Text                          = reader.GetString(reader.GetOrdinal("comment"));

               }
              
            }

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.BufferOutput = true;
            Response.Redirect("ControlMainPage.aspx",false);
        }

        protected void UpdateButton_Click(object sender, EventArgs e)
        {
                     
            DateTime trippingDate1;
            DateTime trippingResotreExpexted;
            DateTime trippingResotreActual;
            String trippingDate             = String.Empty;
            String trippingTime             = String.Empty;
            String causeOfTripping          = String.Empty;
            String actualRestorationTime    = String.Empty;
            String expectedRestorationTime  = String.Empty;
            String actualRestorationDate    = String.Empty;
            String expectedRestorationDate  = String.Empty;
            String networkArea              = String.Empty;
            String votageLevel              = String.Empty;
            String trippingDescription      = String.Empty;
            String substation               = String.Empty;
            String ciricutEquipment         = String.Empty;
            String lostSupplyArea           = String.Empty;
            String powerLostVolume          = String.Empty;
            String protectionRelay          = String.Empty;
            String comment                  = String.Empty;
            int trippingReportNumber        = 0;
            //int badgeNumberInt              = 0;
            int numberOfEffectedConsumers   = 0;
            

            try {

            trippingDate1               = Convert.ToDateTime(trippingReportDateTextBox.Text);
            trippingResotreExpexted     = Convert.ToDateTime(expectedRestorationDateTextBox.Text);
            trippingResotreActual       = Convert.ToDateTime(actualRestorationDateTextBox.Text);

            trippingReportNumber        = int.Parse (TrippingReportLabel.Text);
         // badgeNumberInt              = int.Parse(badgeNumber);
            trippingDate                = string.Format("{0:yyyy-MM-dd}",trippingDate1);
            trippingTime                = string.Format("{0:HH:mm:ss}",trippingTimeTextBox.Text);
            causeOfTripping             = causeOfTrippingTextBox.Text;

            actualRestorationTime       = actualRestorationTimeTextBox.Text;
            expectedRestorationTime     = expectedResotrationTimeTextBox.Text;

            actualRestorationDate       = string.Format("{0:yyyy-MM-dd}", trippingResotreActual);
            expectedRestorationDate     = string.Format("{0:yyyy-MM-dd}", trippingResotreExpexted);

            networkArea                 = Convert.ToString(NetworkAreaDropDownList.SelectedItem);
            votageLevel                 = Convert.ToString(VoltageLevelDropDownList.SelectedItem);
            trippingDescription         = trippingDescriptionTextBox.Text;
            substation                  = Convert.ToString(SubstationDropDowmList.SelectedItem);
            ciricutEquipment            = Convert.ToString(CircuitEquipmentDropDownList.SelectedItem);
            lostSupplyArea              = Convert.ToString(LostSupplyAreaDropDownList.SelectedItem);
            powerLostVolume             = Convert.ToString(PowerLostVolumeTextBox.Text);
            numberOfEffectedConsumers   = int.Parse (NumberOfEffectedConsumersTextBox.Text);
            protectionRelay             = ProtectionRelayTextBox0.Text;
            comment                     = commentTextBox.Text;
                
            String connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            conn.Open();
            queryStr = "update Turkif.TrippingReport " +
                        "set dateOfTripping=        '" + trippingDate +             "',timeOfTripping='" + trippingTime +   "',causeOfTripping='" + causeOfTripping + "' " +
                        ",restorationTimeActual=    '" + actualRestorationTime +    "',restorationTimeExpected='" + expectedRestorationTime + "'" +
                        ",restorationDateActual=    '" + actualRestorationDate +    "',restorationDateExpected='" + expectedRestorationDate + "'" +
                        ",netWorkArea=              '" + networkArea + "',voltagelevel='" + votageLevel + "',descriptionOfTripping='" + trippingDescription + "'" +
                        ",substation=               '" + substation + "',circuitOrEquipment='" + ciricutEquipment + "',areaOfLostSupply='" + lostSupplyArea + "'" +
                        ",volumOfPowerLost=         '" + powerLostVolume + "',noOfConsumerEffected='" + numberOfEffectedConsumers + "'" +
                        ",protectionRelay=          '" + protectionRelay + "',comment='" + comment + "' " +
                        "where trippingReportNo=    '" + trippingReportNumber + "';";
                
             
            
            cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
            cmd.ExecuteReader();
            conn.Close();

            }

            catch (Exception ex) 
            {
                errorMessageLabel.Text = ex.ToString();
            }
            Session["messageOfSucess"] = "Record has been successfully created";
            Response.BufferOutput = true;
            Response.Redirect("ControlMainPage.aspx", false);
        }
        }
   
    }

