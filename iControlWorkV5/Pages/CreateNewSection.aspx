﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="CreateNewSection.aspx.cs" Inherits="iControlWorkV5.Pages.CreateNewSection" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div>
       
        <div>
            <p><asp:Label ID="userNameLabel" runat="server" Text="Label"></asp:Label><asp:Label ID="badgeNoLabel" runat="server" Text="Label"></asp:Label></p>
        </div>
        
         <div>

             <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="sectionNo" DataSourceID="SqlDataSource1">
                 <Columns>
                     <asp:BoundField DataField="SectionName" HeaderText="SectionName" SortExpression="SectionName" />
                     <asp:BoundField DataField="divisionName" HeaderText="divisionName" SortExpression="divisionName" />
                     <asp:BoundField DataField="departmentName" HeaderText="departmentName" SortExpression="departmentName" />
                     <asp:CommandField ShowSelectButton="True" ShowDeleteButton="True" ShowEditButton="True" />
                 </Columns>
             </asp:GridView>


             <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
                 SelectCommand="SELECT S.sectionNo, S.sectionName, V.divisionName, D.departmentName From Department D, Division V, Section S 
                    Where S.divisionNo = V.divisionNo and S.departmentNo=D.departmentNo order by D.departmentName"
                 UpdateCommand = "UPDATE  Section SET  sectionName = @sectionName WHERE sectionNo = @sectionNo "
                 DeleteCommand = "DELETE FROM Section WHERE sectionNo = @sectionNo">

                <UpdateParameters>
                    <asp:Parameter Name="sectionName" Type="String" />
                    <asp:Parameter Name="sectionNo" Type="Int32" />
                </UpdateParameters>

                <DeleteParameters>
                    <asp:Parameter Name="sectionNo" Type="Int32" />
                </DeleteParameters>

     


             </asp:SqlDataSource>


         </div>


         <p><asp:Label ID="deparmtnetSelectLabel" runat="server" Text="Select a department that the division will belong to"></asp:Label></p>
        
         <p><asp:DropDownList ID="departmentDropDownList" runat="server"  AutoPostBack="True" OnSelectedIndexChanged ="DepartmentSelection_SelectedIndexChanged"> 
        <asp:ListItem Text = "--Select Department--" Value = "" ></asp:ListItem>
             </asp:DropDownList> </p>
             <p><asp:Label ID="divisionSelectLabel" runat="server" Text="Select a division that the section will belong to"></asp:Label></p>
        
         <p><asp:DropDownList ID="divisionDropDownList" runat="server"  AutoPostBack="True" >
        <asp:ListItem Text = "--Select Division--" Value = ""></asp:ListItem>

           </asp:DropDownList> </p>
        <p><asp:Label ID="newDivisionLabel" runat="server" Text="Enter The Name of Division"></asp:Label></p>
        <p>
            <asp:TextBox ID="newSectionTextBox" runat="server"></asp:TextBox></p>
        <p><asp:Label ID="returnedMessageLabel" runat="server" Text=""></asp:Label></p>
        <p>
            <asp:Button ID="createNewDivisionButton" runat="server" Text="Create" OnClick="createNewSectionButton_Click" /></p>
        <asp:Button ID="cancelButton" runat="server" Text="Cancel" />

    </div>
</asp:Content>
