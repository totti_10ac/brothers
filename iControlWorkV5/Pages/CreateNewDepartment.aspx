﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="CreateNewDepartment.aspx.cs" Inherits="iControlWorkV5.Pages.CreateNewDepartment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    
        <p><asp:Label ID="userLabel" runat="server" Text="user Name"></asp:Label></p>
    <p><asp:Label ID="badgeNoLabel" runat="server" Text="Badge No"></asp:Label></p>

    </div>
    
    <div>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="departmentNo" DataSourceID="Department">
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="departmentNo" HeaderText="Department No" InsertVisible="False" ReadOnly="True" SortExpression="departmentNo" />
            <asp:BoundField DataField="departmentName" HeaderText="Department Name" SortExpression="departmentName" />
            
<%-- Here is the addition --%>
       
        <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true"

            ItemStyle-Width="100" />

    
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource ID="Department" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
        SelectCommand="SELECT * From Department" 
        UpdateCommand ="UPDATE  Department SET  departmentName = @departmentName WHERE departmentNo = @departmentNo "
        DeleteCommand="DELETE FROM Department WHERE departmentNo = @departmentNo">
        
        <UpdateParameters>
        <asp:Parameter Name="departmentName" Type="String" />
        <asp:Parameter Name="departmentNo" Type="Int32" />
        </UpdateParameters>

        <DeleteParameters>
        <asp:Parameter Name="departmentNo" Type="Int32" />
        </DeleteParameters>
       
    </asp:SqlDataSource>


    </div>
   
    <div style="width: 421px; border-style: ridge; background-color: #FFCC66">
      <p style="width: 136px"> <asp:Label ID="departmentNameLabel" runat="server" Text="Department Name"></asp:Label></p>
     
       <p style="width: 301px"> <asp:TextBox ID="newDepartmentTextBox" runat="server" Width="372px"></asp:TextBox></p>

    
    <asp:Button ID="createNewDepartmentButton" runat="server" Text="Create" OnClick="createNewDepartmentButton_Click" />

    
    <asp:Button ID="cancelButton" runat="server" Text="Cancel" />
    </div>
    
</asp:Content>
