﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="CreateNewDivision.aspx.cs" Inherits="iControlWorkV5.Pages.CreateNewDivision" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <p>
            <asp:Label ID="userNameLabel" runat="server" Text="User Name"></asp:Label><asp:Label ID="badgeNoLabel" runat="server" Text="Badge Number"></asp:Label></p>
        <div>

            <asp:GridView ID="divisionGridView" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="divisionNo" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:BoundField DataField="departmentName" HeaderText="Department Name" SortExpression="departmentName" />
                    <asp:BoundField DataField="divisionNo" HeaderText="Division No" SortExpression="divisionNo" />
                    <asp:BoundField DataField="divisionName" HeaderText="Division Name" SortExpression="divisionName" />
                    <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true"

            ItemStyle-Width="100" />
                </Columns>
            </asp:GridView>

            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
                SelectCommand="Select  departmentName,divisionNo ,divisionName from Division, Department where Department.departmentNo = Division.departmentNo Order by departmentName, divisionName"
                UpdateCommand="Update Division Set divisionName = @divisionName where divisionNo=@divisionNo" 
                DeleteCommand="DELETE FROM Division WHERE divisionNo = @divisionNo">
                <UpdateParameters>
                <asp:Parameter Name="divisionName" Type="String" />
                <asp:Parameter Name="divisionNo" Type="Int32" />
                </UpdateParameters>

                 <DeleteParameters>
               
                <asp:Parameter Name="divisionNo" Type="Int32" />
                </DeleteParameters>

                </asp:SqlDataSource>





        </div>
        
        <div>
        <p><asp:Label ID="deparmtnetSelectLabel" runat="server" Text="Select a department that the division will belong to"></asp:Label></p>
        <p><asp:DropDownList ID="departmentDropDownList" runat="server"  AutoPostBack="True">
            <asp:ListItem Text = "--Select Department--" Value = ""></asp:ListItem>

           </asp:DropDownList> </p>
        <p><asp:Label ID="newDivisionLabel" runat="server" Text="Enter The Name of Division"></asp:Label></p>
        <p>
            <asp:TextBox ID="newDivisionTextBox" runat="server"></asp:TextBox></p>
        <p><asp:Label ID="returnedMessageLabel" runat="server" Text=""></asp:Label></p>
        <p>
            <asp:Button ID="createNewDivisionButton" runat="server" Text="Create" OnClick="createNewDivisionButton_Click" /></p>
        <asp:Button ID="cancelButton" runat="server" Text="Cancel" />
        </div>
    </div>
</asp:Content>
