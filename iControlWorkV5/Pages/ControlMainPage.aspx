﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/ControlEngineer.Master" AutoEventWireup="true" CodeBehind="ControlMainPage.aspx.cs" Inherits="iControlWorkV5.Pages.ControlMainPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>

           <a href="#">Home</a> |  <a href="TrippingReport.aspx">Create new Tripping Report</a> 

            </div>
    <div>

        <asp:Label ID="userNameLabel" runat="server" Text="User Name"></asp:Label>
        <asp:Label ID="badgeNoLabel" runat="server" Text="badgeNo"></asp:Label>

    </div>

    <div>
        <asp:GridView ID="trippingReportGridView" runat="server" DataSourceID="SqlDataSource1"  AllowPaging="True" AllowSorting="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" OnSelectedIndexChanged="SelectingRowToViewATrippingReport">
      


            <Columns>
                <asp:CommandField ShowSelectButton="True" />
              
            </Columns>
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#242121" />


        </asp:GridView>
        
        

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
            SelectCommand="Select trippingReportNo, dateOfTripping, netWorkArea, descriptionOfTripping, substation, comment FROM TrippingReport where (badgeNo= @badgeNo)">
            <SelectParameters> 
                
                <asp:SessionParameter DefaultValue="0" Name="badgeNo" SessionField="badgeNumber" Type="Int32" />
             
            </SelectParameters>
                
        </asp:SqlDataSource>

    </div>
</asp:Content>
