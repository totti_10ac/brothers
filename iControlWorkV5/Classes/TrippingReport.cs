﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iControlWorkV5.Classes
{
    public class TrippingReport
    {
        public int trippingReportNo { get; set; }
        public int badgeNo { get; set; }
        public System.DateTime dateOfTripping { get; set; }
        public System.DateTime timeOfTripping { get; set; }
        public string causeOfTripping { get; set; }
        public Nullable<System.DateTime> resotrationTimeActual { get; set; }
        public Nullable<System.DateTime> restorationTimeExpected { get; set; }
        public Nullable<System.DateTime> restorationDateActual { get; set; }
        public Nullable<System.DateTime> restorationDateExpected { get; set; }
        public string netWorkArea { get; set; }
        public Nullable<decimal> voltagelevel { get; set; }
        public string descriptionOfTripping { get; set; }
        public Nullable<int> substation { get; set; }
        public string circuitOrEquipment { get; set; }
        public string areaOfLostSupply { get; set; }
        public Nullable<decimal> volumOfPowerLost { get; set; }
        public Nullable<int> noOfConsumerEffected { get; set; }
        public string protectionRelay { get; set; }
        public string comment { get; set; }

        public virtual User User { get; set; }
    }
}