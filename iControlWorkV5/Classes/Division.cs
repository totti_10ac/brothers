﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iControlWorkV5.Classes
{
    public class Division
    {
        public Division()
        {
            this.Users = new HashSet<User>();
            this.Sections = new HashSet<Section>();
        }

        public int divisionNo { get; set; }
        public string divisionName { get; set; }
        public Nullable<int> departmentNo { get; set; }

        public virtual Department Department { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Section> Sections { get; set; }

        public String CreateNewDivsion(int departmentNo_In, String divisionName_In)

        {

            String messageReturn = String.Empty;
            divisionName = divisionName_In;
            departmentNo = departmentNo_In;

            try
            {
                MySql.Data.MySqlClient.MySqlConnection conn;
                MySql.Data.MySqlClient.MySqlCommand cmd;
                String queryStr = String.Empty;


                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();
                queryStr = "INSERT INTO Turkif.Division (divisionName, departmentNo) Values('" + divisionName + "', '" +departmentNo+"')";
                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
                cmd.ExecuteReader();
                conn.Close();
            }

            catch (Exception ex)
            {
                messageReturn = Convert.ToString(ex);
            }

            return messageReturn;
        
        
        
        }
    }



   
}