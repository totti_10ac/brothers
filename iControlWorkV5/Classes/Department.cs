﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace iControlWorkV5.Classes
{
    public class Department
    {
           public Department()
        {
            this.Users = new HashSet<User>();
            this.Divisions = new HashSet<Division>();
            this.Sections = new HashSet<Section>();
        }
    
        public int departmentNo { get; set; }
        public string departmentName { get; set; }
    
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Division> Divisions { get; set; }
        public virtual ICollection<Section> Sections { get; set; }


        public String CreateDepartment(String departmentName_In)
        {
            String messageReturn = String.Empty;
            departmentName = departmentName_In;

            try
            {
                MySql.Data.MySqlClient.MySqlConnection conn;
                MySql.Data.MySqlClient.MySqlCommand cmd;
                String queryStr = String.Empty;


                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();
                queryStr = "INSERT INTO Turkif.Department (departmentName) Values('" + departmentName + "')";
                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
                cmd.ExecuteReader();
                conn.Close();
            }

            catch (Exception ex)
            {
                messageReturn = Convert.ToString(ex);
            }

            return messageReturn;
        
        }



    }
}