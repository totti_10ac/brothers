﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace iControlWorkV5
{
    public class User : System.Web.UI.Page
    {
        public User()
        {
            /*this.MobileNoes = new HashSet<MobileNo>();
            this.TrippingReports = new HashSet<TrippingReport>();*/
        }

        public int badgeNo;
        public string firstName;
        public string middleInitial;
        public string lastName;
        public string password;
        public int active;
        public int directNumber;
        public int officeNumber;
        public int roleNo; 
        public int departmentNo; 
        public int divisionNo;
        public int sectionNo;
        public int mobileNo1;
        public int mobileNo2;


        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        String queryStr;
        String userName = String.Empty;
        String badgeNumberString = String.Empty;
        String messageOfLogging = String.Empty;
       // int roleType = 0;
        int badgeNumber = 0;     
        //int activeUser = 0;

        /* public virtual Department Department { get; set; }
         public virtual Division Division { get; set; }
         public virtual ICollection<MobileNo> MobileNoes { get; set; }
         public virtual Role Role { get; set; }
         public virtual Section Section { get; set; }
         public virtual ICollection<TrippingReport> TrippingReports { get; set; }*/


        /*
         * Metho to login into the system
         * */


        public String PerformQuery(String userName, String password)
        {
            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();
                queryStr = "select * from  Turkif.User where badgeNo = '" + userName + "' And password='" + password + "'";
                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
                reader = cmd.ExecuteReader();
                while (reader.HasRows && reader.Read())
                {
                    userName = reader.GetString(reader.GetOrdinal("firstName")) + " " +
                                            reader.GetString(reader.GetOrdinal("middleInitial")) + " " +
                                            reader.GetString(reader.GetOrdinal("lastName"));
                    roleNo = reader.GetInt16(reader.GetOrdinal("roleNo"));
                    badgeNumber = reader.GetInt32(reader.GetOrdinal("badgeNo"));
                    badgeNumberString = Convert.ToString(badgeNumber);
                    active = reader.GetInt16(reader.GetOrdinal("active"));

                }


                String roleNo_String = Convert.ToString(roleNo);
                //Admin and Shift in Charge login since the roleNo for the admin and shift in Cahrge in the DB =1 and 2 respectively.
              
                
                if (reader.HasRows && (roleNo == 1 || roleNo == 2) && active == 1)
                {
                    
                    Session["uname"] = userName;
                    Session["badgeNumber"] = badgeNumberString;
                    Session["roleNumber"] = roleNo_String;
                    HttpContext.Current.Response.BufferOutput = true;
                    HttpContext.Current.Response.Redirect("Pages/AdminMainPage.aspx", false);
                }

                if (reader.HasRows && (roleNo == 3) && active == 1)
                {
                    Session["uname"] = userName;
                    Session["badgeNumber"] = badgeNumberString;
                    Session["roleNumber"] = roleNo_String;
                    HttpContext.Current.Response.BufferOutput = true;
                    HttpContext.Current.Response.Redirect("Pages/ControlMainPage.aspx", false);
                }

                if (reader.HasRows && (roleNo > 3) && active == 1)
                {
                    Session["uname"] = userName;
                    Session["badgeNumber"] = badgeNumberString;
                    Session["roleNumber"] = roleNo_String;
                    HttpContext.Current.Response.BufferOutput = true;
                    HttpContext.Current.Response.Redirect("Pages/MangerMainPage.aspx", false);
                }

                else
                {
                    messageOfLogging = "Invalid Username or Password!";
                }
          
             

                reader.Close();
                conn.Close();
            }

            catch (Exception e)
            {
                messageOfLogging = e.ToString();
            }

          

            return messageOfLogging;
        }
        
        
        /*
         * Method to create new user
         * 
         */
        
        public String CreateNewUser(   String badgeNo_In, String firstName_In, String middleName_In, String lastName_In, String password_In,
                                        String active_In, String directNumber_In, String officeNumber_In, String roleNo_In, String departmentNo_In,
                                        String divisionNo_In, String sectionNo_In, String mobileNo1_In, String mobileNo2_In)
        {

            String messageReturn = String.Empty;
            try
            {
               

                badgeNo = Convert.ToInt32(badgeNo_In);
                firstName = firstName_In;
                middleInitial = middleName_In;
                lastName = lastName_In;
                password = password_In;
                active = Convert.ToInt32(active_In);
                directNumber = Convert.ToInt32(directNumber_In);
                officeNumber = Convert.ToInt32(officeNumber_In);
                roleNo = Convert.ToInt32(roleNo_In);
                departmentNo = Convert.ToInt32(departmentNo_In);
                divisionNo = Convert.ToInt32(divisionNo_In);
                sectionNo = Convert.ToInt32(sectionNo_In);
                mobileNo1 = Convert.ToInt32(mobileNo1_In);
                mobileNo2 = Convert.ToInt32(mobileNo2_In);
            }

            catch (Exception ex2)
            {

                messageReturn = ex2.ToString();  

            }

            try
            {
                MySql.Data.MySqlClient.MySqlConnection conn;
                MySql.Data.MySqlClient.MySqlCommand cmd;
                String queryStr = String.Empty;


                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();
                queryStr = "INSERT INTO Turkif.User (badgeNo, firstName, middleInitial,lastName,password,active,directNumber,officeNumber,roleNo,departmentNo,divisionNo,sectionNo,MobileNo1,MobileNo2)" +
                
                    "Values('" + badgeNo + "','" + firstName + "','" + middleInitial + "','" + lastName + "','" + password + "','" + active + "','" + directNumber + "','" + officeNumber + "','" + roleNo + 
                    "','" + departmentNo + "','" + divisionNo + "','" + sectionNo + "' , '"+ mobileNo2 +"','"+ mobileNo2 +"')";
                 
                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
                cmd.ExecuteReader();
                conn.Close();
               
            }

            catch (Exception ex) 
            {
                messageReturn = Convert.ToString(ex);
            }
           
            


            return messageReturn;
        }

    }
}