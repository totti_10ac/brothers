﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iControlWorkV5.Classes
{
    public class Section
    {

        public Section()
        {
            this.Users = new HashSet<User>();
        }

        public int sectionNo { get; set; }
        public string sectionName { get; set; }
        public Nullable<int> divisionNo { get; set; }
        public Nullable<int> departmentNo { get; set; }

        public virtual Department Department { get; set; }
        public virtual Division Division { get; set; }
        public virtual ICollection<User> Users { get; set; }



        public String CreateNewSection(int departmentNo_In, int divisionNo_In, String sectionName_In)
        {

            String messageReturn = String.Empty;
           
            departmentNo = departmentNo_In;
            divisionNo = divisionNo_In;
            sectionName = sectionName_In;

            try
            {
                MySql.Data.MySqlClient.MySqlConnection conn;
                MySql.Data.MySqlClient.MySqlCommand cmd;
                String queryStr = String.Empty;


                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();
                queryStr = "INSERT INTO Turkif.Section (sectionName, divisionNo,departmentNo) Values('" + sectionName + "', '" + divisionNo + "','" + departmentNo + "')";
                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
                cmd.ExecuteReader();
                conn.Close();
            }

            catch (Exception ex)
            {
                messageReturn = Convert.ToString(ex);
            }

            return messageReturn;



        }
    }
}