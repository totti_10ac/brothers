﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iControlWorkV5.Classes
{
    public class DailySummary
    {
        public System.DateTime dateOfSummary { get; set; }
        public Nullable<System.TimeSpan> timeOfSummary { get; set; }
        public Nullable<decimal> maxTemperature { get; set; }
        public Nullable<decimal> minTemperature { get; set; }
        public Nullable<decimal> maxPowerDemand { get; set; }
        public Nullable<decimal> minPowerDemand { get; set; }
        public string fullNameOfFirstEngineer { get; set; }
        public string fullNameOfSecondEngineer { get; set; }
        public string sectionHeadFullName { get; set; }
        public string divisionManagerFullName { get; set; }
        public string departmentManagerFullName { get; set; }
    }
}