﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iControlWorkV5.Classes
{
    public class Logging : System.Web.UI.Page
    {


        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        String queryStr;
        String userName = String.Empty;
        int roleType = 0;
        int badgeNumber = 0;
        String badgeNumberString = String.Empty;
        String messageOfLoggining = String.Empty;

      

        public String PerformQuery(String userName, String password)
        {
            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();

                queryStr = "select * from  Turkif.User where badgeNo = '" + userName+ "' And password='" + password + "'";

                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);

                reader = cmd.ExecuteReader();

                while (reader.HasRows && reader.Read())
                {
                    userName = reader.GetString(reader.GetOrdinal("firstName")) + " " +
                                            reader.GetString(reader.GetOrdinal("middleInitial")) + " " +
                                            reader.GetString(reader.GetOrdinal("lastName"));
                    roleType = reader.GetInt16(reader.GetOrdinal("roleNo"));
                    badgeNumber = reader.GetInt32(reader.GetOrdinal("badgeNo"));
                    badgeNumberString = Convert.ToString(badgeNumber);

                }

                //Admin login since the roleNo for the admin in the DB =1
                if (reader.HasRows && roleType == 1)
                {
                    
                    Session["uname"] = userName;
                    Session["badgeNumber"] = badgeNumberString;
                    Response.BufferOutput = true;
                    Response.Redirect("Pages/AdminMainPage.aspx", false);

                }

                if (reader.HasRows && roleType == 3)
                {

                    Session["uname"] = userName;
                    Session["badgeNumber"] = badgeNumberString;
                    Response.BufferOutput = true;
                    Response.Redirect("ControlMainPage.aspx", false);

                }

                if (reader.HasRows && (roleType > 3))
                {
                    Session["uname"] = userName;
                    Session["badgeNumber"] = badgeNumberString;
                    Response.BufferOutput = true;
                    Response.Redirect("MangerMainPage.aspx", false);


                }

                else
                {

                    messageOfLoggining = "Invalid Username or Password!";
                }
                reader.Close();
                conn.Close();
            }


            catch (Exception e)
            {
                messageOfLoggining = e.ToString();
            }

            return messageOfLoggining;

        }

    }

    
}