﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iControlWorkV5.Classes
{
    public class Role
    {

        public Role()
        {
            this.Users = new HashSet<User>();
        }

        public int roleNo { get; set; }
        public string roleName { get; set; }

        public virtual ICollection<User> Users { get; set; }
    


    }
}