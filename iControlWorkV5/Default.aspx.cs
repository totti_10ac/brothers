﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace iControlWorkV5
{
    public partial class Default : System.Web.UI.Page
    {
       
        String userName = String.Empty;
        String badgeNumberString = String.Empty;
        String messageOfLogging = String.Empty;
        
        /*
         * Method of loading page
         * 
         * */
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        
        /* Method to first call a function CheckAgainstWhiteList to check the type of characters entered
         * Only the following charachter is accepted aA-zZ 0-9
         *
         */
        public void submitEventMethod(object sender, EventArgs e)
        {
            User User = new User();

            if (CheckAgaintstWhiteList(usernameTextBox.Text) == true &&
                CheckAgaintstWhiteList(passwordTextBox.Text) == true)
            {              
               messageOfLogging = User.PerformQuery(usernameTextBox.Text, passwordTextBox.Text);             
            }

            else
            {
                messageOfLogging = "Does not pass white list test";
            }

            errorMessageLabel.Text = messageOfLogging;
            passwordTextBox.Text = "";
        }
     
        /*
         * Checking against White List characters
         * */
        private bool CheckAgaintstWhiteList(String userInput)
        {
            var regularExperssion = new System.Text.RegularExpressions.Regex(@"^[a-zA-Z0-9]*$_*");
            if (regularExperssion.IsMatch(userInput))
            {
                return true;
            }
            else
            {
                return false;
            }
         }

      

    }
}
